package hashTable;

public abstract class AbstractHashTable implements HashTable {
  public abstract int getLength();

  protected final int f(Object object) {
    return object.hashCode();
  }

  protected final int g(int x) {
    return Math.abs(x) % getLength();
  }

  protected final int h(Object object) {
    return g(f(object));
  }

  @Override
  public double getLoadFactor() {
    // TODO Auto-generated method stub
    return (double) getCount() / getLength();
  }
}
