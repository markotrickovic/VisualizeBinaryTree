package hashTable;

public class ChainedScatterTable extends AbstractHashTable {
  protected Entry[] array;
  static final int nil = -1;

  protected static final class Entry {
    Object object;
    int next = nil;

    void delete() {
      object = null;
      next = nil;
    }
  }

  public ChainedScatterTable(int length) {
    array = new Entry[length];
    for (int i = 0; i < length; ++i) {
      array[i] = new Entry();
    }
  }

  @Override
  public int getLength() {
    // TODO Auto-generated method stub
    return array.length;
  }

  public void delete() {
    for (int i = 0; i < getLength(); ++i) {
      array[i].delete();
    }
    count = 0;
  }

  public void withdraw(Object obj) {
    if (count == 0) {
      throw new TableEmptyException();
    }
    int i = h(obj);
    while (i != nil && obj != array[i].object) {
      i = array[i].next;
    }
    if (i == nil) {
      throw new IllegalArgumentException("Indeks van opsega!");
    }
    for (;;) {
      int j = array[i].next;
      while (j != nil) {
        int h = h(array[j].object);
        boolean contained = false;
        for (int k = array[i].next; k != array[j].next && !contained; k = array[k].next) {
          if (k == h) {
            contained = true;
          }
        }
        if (!contained) {
          break;
        }
        j = array[j].next;
      }
      if (j == nil)
        break;
      array[i].object = array[j].object;
      i = j;
    }
    array[i].object = nil;
    array[i].next = nil;
    for (int j = (i + getLength() - 1) % getLength(); j != i; j = (j + getLength() - 1)
            % getLength()) {
      if (array[j].next == i) {
        array[j].next = nil;
        break;
      }
    }
    --count;
  }

  public void insert(Object obj) {
    if (count == getLength()) {
      throw new TableFullException();
    }
    int probe = h(object);
    if (array[probe].object != null) {
      while (array[probe].next != nil) {
        probe = array[probe].next;
      }
      int tail = probe;
      probe = (probe + 1) % getLength();
      while (array[probe].object != null) {
        probe = (probe + 1) % getLength();
      }
      array[tail].next = probe;
    }
    array[probe].object = obj;
    array[probe].next = nil;
    ++count;
  }

  public Object find(Object object) {
    for (int probe = h(obj); probe != nil; probe = array[probe].next) {
      if (object.isEqual(array[probe].object)) {
        return array[probe].object;
      }
    }
    return null;
  }
}
