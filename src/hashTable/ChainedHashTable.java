package hashTable;

import com.sun.tools.javac.code.Attribute.Array;

public class ChainedHashTable extends AbstractHashTable {
  protected Array[] array;

  public ChainedHashTable(int length) {
    array = new LinkedList[length];
    for (int i = 0; i < length; i++) {
      array[i] = new LinkedList();
    }
  }

  @Override
  public int getLength() {
    // TODO Auto-generated method stub
    return array.length;
  }

  public void delete() {
    for (int i = 0; i < getLength(); ++i) {
      array[i].delete();
    }
    count = 0;
  }

  public void insert(Object obj) {
    array[h(obj)].append(obj);
    ++count;
  }

  public void withdraw(Object obj) {
    array[h(obj)].append(obj);
    ++count;
  }

  public Object find(Object obj) {
    for (LinkedList.Element ptr = array[h(object)].getHead(); ptr != null; ptr = ptr.getNext()) {
      Object datum = (Object) ptr.getDatum();
      if (object.isEqual(datum)) {
        return datum;
      }
    }
    return null;
  }
}
