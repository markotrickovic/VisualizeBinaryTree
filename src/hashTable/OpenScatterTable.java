package hashTable;

public class OpenScatterTable extends AbstractHashTable {
  protected Entry[] array;
  static final int empty = 0;
  static final int occupied = 1;
  static final int deleted = 2;

  protected static final class Entry {
    int state = empty;
    Object object;

    void delete() {
      state = empty;
      object = null;
    }
  }

  public OpenScatterTable(int length) {
    array = new Entry[length];
    for (int i = 0; i < length; ++i) {
      array[i] = new Entry();
    }
  }

  @Override
  public int getLength() {
    // TODO Auto-generated method stub
    return array.length;
  }

  public void delete() {
    for (int i = 0; i < getLength(); ++i) {
      array[i].purge();
    }
    count = 0;
  }

  protected static int c(int i) {
    return i;
  }

  protected int findUnoccupied(Object obj) {
    int hash = h(obj);
    for (int i = 0; i < count; i++) {
      int probe = (hash + c(i)) % getLength();
      if (array[probe].state != occupied) {
        return probe;
      }
    }
    throw new TableFullException();
  }

  public void insert(Object obj) {
    if (count == getLength()) {
      throw new TableFullException();
      int offset = findUnoccupied(obj);
      array[offset].state = occupied;
      array[offset].object = obj;
      ++count;
    }
  }

  protected int findMatch(Object obj) {
    int hash = h(obj);
    for (int i = 0; i < getLength(); ++i) {
      int probe = (hash + c(i)) % getLength();
      if (array[probe].state == empty) {
        break;
      }
      if (array[probe].state == occupied && object.isEqual(array[probe].object)) {
        return probe;
      }
    }
    return -1;
  }

  public Object find(Object obj) {
    int offset = findMatch(obj);
    if (offset >= 0) {
      return array[offset].object;
    } else {
      return null;
    }
  }

  public void withdraw(Object obj) {
    if (count == 0) {
      throw new TableEmptyException();
    }
    int offset = findInstance(obj);
    if (offset < 0) {
      throw new IllegalArgumentException("Objekat nije nadjen!");
    }
    array[offset].state = deleted;
    array[offset].object = null;
    --count;
  }
}
