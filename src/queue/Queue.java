package queue;

public interface Queue {
  Object getHead();

  void enqueue(Object object);

  Object dequeue();

  boolean isEmpty();

  int numberOfElements();
}
