package queue;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;

public class QueueAsArray implements Queue {
  protected Object[] array;
  protected int head;
  protected int tail;
  protected int numOfElements;

  public QueueAsArray(int size) {
    array = new Object[size];
    head = 0;
    tail = size - 1;
    numOfElements = 0;
  }

  @Override
  public Object getHead() {
    // TODO Auto-generated method stub
    if (numOfElements == 0) {
      throw new BufferUnderflowException();
    }
    return array[head];
  }

  @Override
  public void enqueue(Object object) {
    // TODO Auto-generated method stub
    if (numOfElements == array.length) {
      throw new BufferOverflowException();
    }
    if (++tail == array.length) {
      tail = 0;
      array[tail] = object;
      ++numOfElements;
    }
  }

  @Override
  public Object dequeue() {
    // TODO Auto-generated method stub
    if (numOfElements == 0) {
      throw new BufferUnderflowException();
    }
    Object result = array[head];
    array[head] = null;
    if (++head == array.length) {
      head = 0;
    }
    --numOfElements;
    return result;
  }

  @Override
  public boolean isEmpty() {
    // TODO Auto-generated method stub
    return (numOfElements == 0);
  }

  @Override
  public int numberOfElements() {
    // TODO Auto-generated method stub
    return numOfElements;
  }

  public void delete() {
    while (numOfElements > 0) {
      array[head] = null;
      if (++head == array.length) {
        head = 0;
      }
      --numOfElements;
    }
  }

}
