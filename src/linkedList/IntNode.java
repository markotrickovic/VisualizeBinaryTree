package linkedList;

public class IntNode {
  public int info;
  public IntNode link;

  public IntNode() {
    this(0, null);
  }

  public IntNode(int i) {
    this(i, null);
  }

  public IntNode(int i, IntNode n) {
    info = i;
    link = n;
  }
}
