package linkedList;

public class IntSLList {
  private IntNode head, tail;

  public IntSLList() {
    tail = null;
    head = tail;
  }

  public boolean isEmpty() {
    return head == null;
  }

  public void addToHead(int el) {
    head = new IntNode(el, head);
    if (tail == null) {
      tail = head;
    }
  }

  public void addToTail(int el) {
    if (!isEmpty()) {
      tail.link = new IntNode(el);
      tail = tail.link;
    } else {
      tail = new IntNode(el);
      head = tail;
    }
  }

  public int deleteFromHead() {
    int el = head.info;
    if (head == tail) {
      tail = null;
      head = tail;
    } else {
      head = head.link;
    }
    return el;
  }

  public int deleteFromTail() {
    int el = tail.info;
    if (head == tail) {
      tail = null;
      head = tail;
    } else {
      IntNode tmp;
      tmp = head;
      tail = tmp;
      tail.link = null;
    }
    return el;
  }

  public void printAll() {
    for (IntNode tmp = head; tmp != null; tmp = tmp.link) {
      System.out.print(tmp.info + " ");
    }
  }

  public boolean isInList(int el) {
    IntNode tmp;
    for (tmp = head; tmp != null && tmp.info != el; tmp = tmp.link)
      ;
    return tmp != null;
  }

  public void delete(int el) {
    if (!isEmpty()) {
      if (head == tail && el == head.info) {
        tail = null;
        head = tail;
      } else if (el == head.info) {
        head = head.link;
      } else {
        IntNode pred, tmp;
        for (pred = head, tmp = head.link; tmp != null
                && tmp.info != el; pred = pred.link, tmp = tmp.link)
          ;
        if (tmp != null) {
          pred.link = tmp.link;
          if (tmp == tail) {
            tail = pred;
          }
        }
      }
    }
  }
}
