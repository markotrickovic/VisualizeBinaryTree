package linkedList;

public class IntSkipListNode {
  public int info;
  public IntSkipListNode[] link;
  
  public IntSkipListNode(int i, int n) {
    info = i;
    next = new IntSkipListNode[n];
    for (int j = 0; j < n; j++) {
      link[j] = null;
    }
  }
}
