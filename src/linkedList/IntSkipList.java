package linkedList;

import java.util.Random;

public class IntSkipList {
  private int maxLevel;
  private IntSkipListNode[] root;
  private int[] powers;
  private Random rd = new Random();

  public IntSkipList() {
    this(4);
  }

  public IntSkipList(int i) {
    maxLevel = i;
    root = new IntSkipListNode[maxLevel];
    powers = new int[maxLevel];
    for (int j = 0; j < maxLevel; j++) {
      root[j] = null;
    }
    choosePowers();
  }

  public boolean isEmpty() {
    return root[0] == null;
  }

  public void choosePowers() {
    powers[maxLevel - 1] = (2 << (maxLevel - 1)) - 1;
    for (int i = maxLevel - 2, j = 0; i >= 0; i--, j++) {
      powers[i] = powers[i + 1] - (2 << j);
    }
  }

  public int chooseLevel() {
    int i, r = Math.abs(rd.nextInt()) % powers[maxLevel - 1] + 1;
    for (i = 1; i < maxLevel; i++) {
      if (r < powers[i]) {
        return i - 1; // vraća nivo niži od najvišeg
      }
    }
    return i - 1; // vraća najviši nivo
  }

  public int skipListSearch(int info) {
    int lvl;
    IntSkipListNode prev, curr; // nalazi najviši nenulti nivo
    for (lvl = maxLevel - 1; lvl >= 0 && rooot[lvl] == null; lvl--)
      ;
    prev = curr = root[lvl];
    while (true) {
      if (info == curr.info) { // element je nađen
        return curr.info;
      } else if (info < curr.info) { // ako je manji prelazi se jedan nivo niže
        if (lvl == 0) { // već smo na najnižem nivou
          return 0;
        } else if (curr == root[lvl]) {
          curr = root[--lvl]; // ako smo na početku
        } else { // ako postoji prethodnik
          curr = prev.link[--lvl];
        }
      } else { // ako je veći prelazi se na sledeći čvor
        prev = curr; // na istom nivou ako nije null ili
        if (curr.link[lvl] != null) { // ili na niži nivo
          curr = curr.link[lvl];
        } else {
          for (lvl--; lvl >= 0 && curr.link[lvl] == null; lvl--)
            ;
          if (lvl >= 0) {
            curr = curr.link[lvl];
          } else {
            return 0;
          }
        }
      }
    }
  }

  public void display() {
    System.out.println("Print list:");
    for (int i = maxLevel - 1; i >= 0; i--) { // stampa nivo po nivo
      for (IntSkipListNode p = root[i]; p != null; p = p.link[i]) {
        System.out.print(p.info + " ");
      }
      System.out.println("|");
    }
  }

  public void skipListInsert(int info) {
    IntSkipListNode[] curr = new IntSkipListNode[maxLevel];
    IntSkipListNode[] prev = new IntSkipListNode[maxLevel];
    IntSkipListNode newNode;
    int lvl, i;
    curr[maxLevel - 1] = root[maxLevel - 1];
    prev[maxLevel - 1] = null;
    for (lvl = maxLevel - 1; lvl >= 0; lvl--) {
      while (curr[lvl] != null && curr[lvl].info < info) {
        prev[lvl] = curr[lvl]; // ako je manji preći na sledeći čvor
        curr[lvl] = curr[lvl].link[lvl];
      }
      if (curr[lvl] != null && curr[lvl].info == info) {
        return; // već postoji čvor sa tim info poljem
      }
      if (lvl > 0) { // preći na niži nivo
        if (prev[lvl] == null) { // polazeći od root-a
          curr[lvl - 1] = root[lvl - 1];
          prev[lvl - 1] = null;
        }
      } else { // ili prethodnog čvora
        curr[lvl - 1] = prev[lvl].link[lvl - 1];
        prev[lvl - 1] = prev[lvl];
      }
    }
    lvl = chooseLevel(); // slučajno generisati nivo novog čvora
    newNode = new IntSkipListNode(info, lvl + 1);
    for (i = 0; i <= lvl; i++) { // inicijalizacija link polja
      newNode.link[i] = curr[i];
      if (prev[i] == null) { // ažuriranje root-a
        root[i] = newNode;
      } else {
        prev[i].link[i] = newNode; // ili prethodnog čvora
      }
    }
  }
}
