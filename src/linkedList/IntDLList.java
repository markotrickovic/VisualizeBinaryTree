package linkedList;

public class IntDLList {
  private IntDLLNode head, tail;
  public IntDLList() {
    tail = null;
    head = tail;
  }
  public boolean isEmpty() {
    return head == null;
  }
  public void setToNull() {
    tail = null;
    head = tail;
  }
  public int firstEl() {
    if (!isEmpty()) {
      return head.info;
    } else {
      return 0;
    }
  }
  public void addToDLListHead(int el) {
    if (!isEmpty()) {
      head = new IntDLLNode(el,head,null);
      head.next.prev = head;
    }
    else {
      tail = new IntDLLNode(el);
      head = tail;
    }
  }
  public void addToDLListTail(int el) {
    if (!isEmpty()) {
      tail = new IntDLLNode(el,null,tail);
      tail.prev.next = tail;
    } else {
      tail = new IntDLLNode(el);
      head = tail;
    }
  }
  public int deleteFromDLListHead() {
    if (!isEmpty()) {
      int el = head.info;
      if (head == tail) {
        tail = null;
        head = tail;
      } else {
        head = head.next;
        head.prev = null;
      }
      return el;
    }
    else {
      return 0;
    }
  }
  
  public int deleteFromDLListTail() {
    if (!isEmpty()) {
      int el = tail.info;
      if (head == tail) {
        tail = null;
        head = tail;
      } else {
        tail = tail.prev;
        tail.next = null;
      }
      return el;
    }
    else {
      return 0;
    }
  }
  
  public void printAll() {
    for (IntDLLNode tmp = head; tmp != null; tmp = tmp.next) {
      System.out.print(tmp.info + " ");
    }
  }
  
  public int find(int el) {
    IntDLLNode tmp;
    for (tmp = head; tmp != null && tmp.info != el; tmp = tmp.next);
    if (tmp == null) {
      return 0;
    } else {
      return tmp.info;
    }
  }
}
