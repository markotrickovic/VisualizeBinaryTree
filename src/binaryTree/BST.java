package binaryTree;

import queue.QueueAsArray;
import stack.StackAsArray;

public class BST {
  protected BSTNode root = null;

  public BST() {}

  public void clear() {
    root = null;
  }

  public boolean isEmpty() {
    return root == null;
  }

  public void insert(int el) {
    BSTNode p = root, prev = null;
    while (p != null) { // traženje mesta za umetanje novog čvora
      prev = p;
      if (p.key < el) {
        p = p.right;
      } else {
        p = p.left;
      }
    }
    if (root == null) { // stablo je prazno
      root = new BSTNode(el);
    } else if (prev.key < el) {
      prev.right = new BSTNode(el);
    } else {
      prev.left = new BSTNode(el);
    }
  }

  public boolean isInTree(int el) {
    return search(el) != null;
  }

  public Object search(int el) {
    return search(root, el);
  }

  protected Object search(BSTNode p, int el) {
    while (p != null) {
      if (el == p.key) {
        return p.key;
      } else if (el < p.key) {
        p = p.left;
      } else {
        p = p.right;
      }
    }
    return null;
  }

  public void preorder() {
    preorder(root);
  }

  public void inorder() {
    inorder(root);
  }

  public void postorder() {
    postorder(root);
  }

  protected void inorder(BSTNode p) {
    if (p != null) {
      inorder(p.left);
      p.visit();
      inorder(p.right);
    }
  }

  protected void preorder(BSTNode p) {
    if (p != null) {
      p.visit();
      preorder(p.left);
      preorder(p.right);
    }
  }

  protected void postorder(BSTNode p) {
    if (p != null) {
      postorder(p.left);
      postorder(p.right);
      p.visit();
    }
  }

  public void deleteByCopying(int el) {
    BSTNode node, p = root, prev = null;
    while (p != null && !(p.key == el)) { // nalaženje čvora sa željenim el.
      prev = p;
      if (p.key < el) {
        p = p.right;
      } else {
        p = p.left;
      }
    }
    node = p;
    if (p != null && p.key == el) {
      if (node.right == null) { // čvor nema desnog potomka (1)
        node = node.left;
      } else if (node.left == null) { // čvor nema levog potomka (2)
        node = node.right;
      } else {
        BSTNode tmp = node.left;
        BSTNode previous = node;
        while (tmp.right != null) { // nalaženje krajnjeg desnog čvora
          previous = tmp; // u levom podstablu
          tmp = tmp.right;
        }
        node.key = tmp.key; // prepisivanje reference na ključ

        if (previous == node) { // tmp je direktni levi potomak node-a
          previous.left = tmp.left; // ostaje isti raspored u levom podstablu
        } else {
          previous.right = tmp.left; // levi potomak tmp-a
          tmp.left = null; // se pomer na mesto tmp-a
        }
      }
      if (p == root) { // u slučaju (1) i (2) samo je pomerena
        root = node; // referenca node pa je potrebno
      } else if (prev.left == p) { // izmeniti i link prethodnog čvora
        prev.left = node; // u slučaju (3) ovo nema dejstva
      } else {
        prev.right = node;
      }
    } else if (root != null) {
      System.out.println("Traženi el." + el + " nije u stablu!");
    } else {
      System.out.println("Stablo je prazno!");
    }
  }

  public void balance(int data[], int first, int last) {
    if (first < last) {
      int middle = (first + last) / 2;
      insert(data[middle]);
      balance(data, first, middle - 1);
      balance(data, middle + 1, last);
    }
  }

  public void deleteByMerging(int el) {
    BSTNode tmp, node, p = root, prev = null;
    while (p != null && !(p.key == el)) { // nalaženje čvora sa željenim el.
      prev = p;
      if (p.key < el) {
        p = p.right;
      } else {
        p = p.left;
      }
    }
    node = p;
    if (p != null && p.key == el) {
      if (node.right == null) { // čvor nema desnog potomka (1)
        node = node.left;
      } else if (node.left == null) { // čvor nema levog potomka (2)
        node = node.right;
      } else { // čvor ima oba potomka (3)
        tmp = node.left;
        while (tmp.right != null) { // nalaženje kranjeg desnog čvora
          tmp = tmp.right; // u levom podstablu
        }
        tmp.right = node.right; // prebacivanje desnog linka node-a u tmp
        node = node.left; // na tekućem mestu biće prvi levi potomak
      }
      if (p == root) {
        root = node;
      } else if (prev.left == p) {
        prev.left = node;
      } else {
        prev.right = node;
      }
    } else if (root != null) {
      System.out.println("Traženi el." + el + " nije u stablu!");
    } else {
      System.out.println("Stablo je prazno!");
    }
  }

  public void breadthFirst() {
    BSTNode p = root;
    QueueAsArray queue = new QueueAsArray(100);
    if (p != null) {
      queue.enqueue(p);
      while (!queue.isEmpty()) {
        p = (BSTNode) queue.dequeue();
        p.visit();
        if (p.left != null) {
          queue.enqueue(p.left);
        }
        if (p.right != null) {
          queue.enqueue(p.right);
        }
      }
    }
  }

  public void iterativePreorder() {
    BSTNode p = root;
    StackAsArray travStack = new StackAsArray(100);
    if (p != null) {
      travStack.push(p);
      while (!travStack.isEmpty()) {
        p = (BSTNode) travStack.pop();
        p.visit();
        if (p.right != null) { // levi potomak se stavlja u magacing
          travStack.push(p.right); // posle desnog, da bi se prvi obradio
        }
        if (p.left != null) {
          travStack.push(p.left);
        }
      }
    }
  }

  public void iterativeInorder() {
    BSTNode p = root;
    StackAsArray travStack = new StackAsArray(100);
    while (p != null) {
      while (p != null) {
        if (p.right != null) {
          travStack.push(p.right); // u magacin se stavlja prvo desni
        }
        travStack.push(p); // pa zatim tekući čvor
        p = p.left; // i prelazi na levog potomka
      }
      p = (BSTNode) travStack.pop(); // čvor bez levih potomaka
      while (!travStack.isEmpty() && p.right == null) {
        p.visit(); // obilazak tekućeg čvora i ostalih
        p = (BSTNode) travStack.pop(); // bez desnih potomaka
      }
      p.visit(); // obilazak prvog čvora
      if (!travStack.isEmpty()) { // i njegovih desnih potomaka
        p = (BSTNode) travStack.pop();
      } else {
        p = null;
      }
    }
  }

  public void iterativePostorder() {
    BSTNode p = root, q = root;
    StackAsArray travStack = new StackAsArray(100);
    while (p != null) {
      for (; p.left != null; p = p.left) {
        travStack.push(p);
      }
      while (p != null && (p.right == null || p.right == q)) {
        p.visit();
        q = p;
        if (travStack.isEmpty()) {
          return;
        }
        p = (BSTNode) travStack.pop();
      }
      travStack.push(p);
      p = p.right;
    }
  }
}
