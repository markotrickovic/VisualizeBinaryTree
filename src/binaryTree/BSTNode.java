package binaryTree;

/** Klasa čvora uređenog binarnog stabla */
public class BSTNode {
  protected int key;
  protected BSTNode left, right;

  /** Podrazumevani konstruktor */
  public BSTNode() {
    left = right = null;
  }

  /** Konstruktor. Kreira čvor i ključu dodeljuje
   * vrednost parametra el.
   */
  public BSTNode(int el) {
    this(el, null, null);
  }

  /** Konstruktor. Kreira čvor, ključu dodeljuje
   * vrednost parametra el, a levom i desnom
   * pokazivaču na potomke dodeljuje reference na
   * čvorove lt i rt respektivno. */
  public BSTNode(int el, BSTNode lt, BSTNode rt) {
    key = el;
    left = lt;
    right = rt;
  }

  /** Virtuelna funkcija za obradu čvora. */
  public void visit() {
    // key.visit();
  }
}
