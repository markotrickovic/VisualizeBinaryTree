package stack;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;

public class StackAsArray implements Stack {
  protected Object[] array;
  protected int top;

  public StackAsArray(int size) {
    array = new Object[size];
    top = -1;
  }

  @Override
  public Object getTop() {
    // TODO Auto-generated method stub
    if (top == -1) {
      throw new BufferUnderflowException();
    }
    return array[top];
  }

  @Override
  public void push(Object object) {
    // TODO Auto-generated method stub
    if (top == (array.length - 1)) {
      throw new BufferOverflowException();
    }
    array[++top] = object;
  }

  @Override
  public Object pop() {
    // TODO Auto-generated method stub
    if (top == -1) {
      throw new BufferUnderflowException();
    }
    Object result = array[top];
    array[top--] = null;
    return result;
  }

  @Override
  public boolean isEmpty() {
    // TODO Auto-generated method stub
    return (top == -1);
  }

  @Override
  public int numberOfElements() {
    // TODO Auto-generated method stub
    return (top + 1);
  }

  public void delete() {
    while (top > -1) {
      array[top--] = null;
    }
  }
}
