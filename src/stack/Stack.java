package stack;

public interface Stack {
  Object getTop();

  void push(Object object);

  Object pop();

  boolean isEmpty();

  int numberOfElements();
}
